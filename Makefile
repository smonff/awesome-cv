.PHONY: examples

CC = xelatex
WORK_DIR = work
COVER_DIR = work/cover
CV_DIR = work/cv
CV_SRCS = $(shell find $(CV_DIR) -name '*.tex')
COVER_SRCS = $(shell find $(COVER_DIR) -name '*.tex')
TIMESTAMP = $(shell date "+%Y-%m-%d")

examples: $(foreach x, coverletter cv, $x.pdf)

cv.pdf: $(WORK_DIR)/cv.tex $(CV_SRCS)
	echo $(TIMESTAMP)
	$(CC) -output-directory=$(WORK_DIR) $<
	mv $(WORK_DIR)/cv.pdf $(WORK_DIR)/cv_sebastien_feugere_$(TIMESTAMP).pdf

coverletter.pdf: $(WORK_DIR)/coverletter.tex
	echo $(TIMESTAMP)
	$(CC) -output-directory=$(WORK_DIR) $<
	mv $(WORK_DIR)/coverletter{,_sebastien_feugere_$(TIMESTAMP)}.pdf

clean:
	rm -rf $(WORK_DIR)/*.pdf

test:
	@echo $(CV_DIR)
	@echo $(CV_SRCS)
	@echo $(WORK_DIR)
	@echo $(TIMESTAMP)
