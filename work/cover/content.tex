\begin{cvletter}

%\lettersection{}

Je souhaiterais proposer ma candidature à Open Food Fact pour le poste de développeur full stack.
J’ai tout d’abord pris connaissance de votre besoin en recrutement dans mes réseaux notamment sur Mastodon, puis j’en ai discuté avec Florentin Raud (kyzh) qui avait travaillé avec vous dans le passé. Il m’a encouragé à postuler en raison de certains centres d’intérêts communs et peut éventuellement vous apporter une recommandation sur certaine de mes compétences.

Je me sens particulièrement approprié pour candidater à ce poste car intégrer une structure associative/domaine public/à but non-lucratif a toujours été un objectif pour moi, malgré que je ne l’ai jamais fait à titre salarié. J’ai cependant travaillé assez longtemps pour la Scop Etyssa, qui est une  coopérative travaillant exclusivement pour le service public. De plus, je suis spécialiste de l’écosystème Perl, qui me semble un prérequis pour travailler sur le cœur de Open Food Facts, ainsi que les langages JavaScript et Java.

J’ai un intérêt certain pour les données ouvertes et le logiciel open source : c’est cette philosophie qui m’a amené à m’intéresser aux cultures ouvertes, à Linux puis au développement alors que j’effectuais mes études à l’École Nationale Supérieur d’Art de Bourges.

Mon activité principale depuis 2009 est le développement web. J’ai essentiellement travaillé pour de petites structures : SARL Scop, startup, développeur au service Production d’un institut de sondages. De par la taille réduite de ces organisations, j’ai pu prendre en charge des rôles assez différents et j’ai beaucoup appris grâce à des personnes qui m’ont fait confiance : développement front-end, sysadmin, sécurité, méthodologie, bases de données, devops, gestion de projets, etc.

Ces quatre dernières années, mon intérêt s’est essentiellement tourné sur le back-end et sur les façons de le faire interagir avec la partie visible des applications. Je me suis diversifié dans mes pratiques de développement, tout en continuant le développement web et surtout la mise en place de microservices, d’API REST. J’ai aussi pris en charge la conception d’infrastructures et l’analyse de données, l’interopérabilité entre applications, API et bases de données disparates, la data-visualisation, la génération de rapports, robots de crawling, etc.

Ce qui m’attire dans une structure non-profit, ce serait de contribuer aux \textit{« communs »} en agissant sur des données ouvertes, qui profitent directement au public, à la recherche. L’aspect d’accessibilité au plus grand nombre et la dimension éducative font également partie de mes préoccupations, c’est à dire que en tant que développeur, je veux me saisir de ces problématiques pour y apporter des solutions simples.

Malgré mon intérêt pour le \textit{« non-commercial »}, en tant que ancien associé de la Scop Etyssa, je suis parfaitement au fait des responsabilités de type budget et plannings. Mes qualités d’organisation ainsi que méthodologiques ont pu être reconnues. J’apprécie grandement le partage de connaissances et le travail collaboratif, j’avoue ne pas trop m’être épanouis lors de ma précédente expérience où toutes les personnes étaient en concurrence à courir après des promotions hypothétiques.

À savoir que je suis actuellement libre de tout engagement contractuel et que je bénéficie d’un CSP, ce qui ouvrirait la porte à Open Food Facts à des possibilités d’aide à la formation (dont du temps pour formation internes peuvant être prises en charge) ainsi que sur des technologies spécifiques complémentaires pouvant manquer à mes compétences.

% Annexes Joindre des liens vers CPAN, site, Codeberg


% OVH est un hébergeur Internet de renommée mondiale aux services innovant et de qualité. Vous recherchez de nouveaux collaborateurs et j'ai été intéressé dans le domaine du développement Perl.

% Depuis une formation d'un an à l'AFPA en 2008, j'ai travaillé dans le domaine du développement web, essentiellement au sein de la SCOP Etyssa qui fournit des services pour les collectivités locales. En 2011, j'ai commencé à m'intéresser à Perl et à son écosystème en développant quelques outils et participant à des conférences et hackathons. Mes expériences professionnelles ne reflètent pas beaucoup cet intérêt, mais j'ai quand même pu prendre en charge un module sur le CPAN et développer quelques outils d'automatisation de backups de bases de données et de test d'APIs. Je suis particulièrement attaché à la culture du Test Driven Development de ce langage.

% Ayant travaillé majoritairement dans des TPEs (moins de 10 personnes), les tâches que j'ai résolues sont assez variées, allant de la maintenance d'un grande application JavaEE au développement front-end, en passant par l'administration de serveurs Linux. À noter que mon intérêt pour l'informatique, le développement date de la période de mes études, vers 2005, dans un contexte où ce n'est pas 'au programme' (secteur artistique). J'ai depuis pris en charge la maintenance de quelques serveurs pour divers projets

%% La CGT est un syndicat historique permettant aux travailleurs de s'organiser afin de défendre leurs droits et de bâtir un monde plus juste. Vous recherchez un.e développeur.e informatique, dans dans le cadre du développement et de la refonte de vos outils partagés.

%% Depuis une formation à l'AFPA en 2008-2009, j'ai travaillé dans le domaine du développement web, essentiellement au sein d'une coopérative (SCOP) fournissant des services pour les collectivités locales.

%% Mon expérience est assez variée, avec des connaissances poussées en programmation objet, en test logiciel dans le cadre de démarches qualité, en développement front-end et en administration de serveurs open-source. Je suis particulièrement attentif à la bonne qualité de mon travail. J'aime les équipes bienveillantes, mixtes et structurées. Je suis rodé à l'échange trans-disciplinaire et tout à fait à même d'échanger avec différents interlocuteur.e.s (par exemple en dehors du service informatique). Actuellement en recherche d'emploi, je suis disponible immédiatement.

%% Travailler pour la CGT serait une occasion unique pour mettre mes compétences au service d'une cause ayant un sens fort. Dans ce sens, je vous invite à examiner cette candidature et je me tiens à votre disposition pour un éventuel entretien.

%% J'ai connu Alpha Taxis en tant que client et garde un très bon souvenir des quelques courses que j'ai effectuées dans vos voitures avec des chauffeurs très professionnels.

%% \lettersection{A propos de moi}
%% Travaillant en SCOP depuis 2013 et intéresse par ce modèle d'entreprise, je m'étais naturellement tourné vers vous, n'étant pas du tout convaincu par le modèle social offert par les VTC de Über et compagnie.

%% Aujourd'hui en recherche d'emploi, je m'adresse à vous afin de vous proposer mes services en tant qu'informaticien. Mon expérience est surtout celle des Très Petites Entreprises, donc cela va du développement (ma compétence principale) à l'administration système en passant par le support utilisateur.

%% Si vous avez des besoins allant dans ce sens pour vos sites web, vos applications mobiles ou vos serveurs, je serai heureux de pouvoir travailler avec vous.

\end{cvletter}
